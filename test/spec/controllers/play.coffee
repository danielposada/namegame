'use strict'

describe 'Controller: PlayCtrl', () ->

  # load the controller's module
  beforeEach module 'nameGameApp'

  PlayCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    PlayCtrl = $controller 'PlayCtrl', {
      $scope: scope
    }
