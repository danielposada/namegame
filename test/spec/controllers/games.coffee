'use strict'

describe 'Controller: GamesCtrl', () ->

  # load the controller's module
  beforeEach module 'nameGameApp'

  GamesCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    GamesCtrl = $controller 'GamesCtrl', {
      $scope: scope
    }

