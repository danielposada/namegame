'use strict'

describe 'Controller: GameCtrl', () ->

  # load the controller's module
  beforeEach module 'nameGameApp'

  GameCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    GameCtrl = $controller 'GameCtrl', {
      $scope: scope
    }

