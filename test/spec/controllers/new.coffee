'use strict'

describe 'Controller: NewCtrl', () ->

  # load the controller's module
  beforeEach module 'nameGameApp'

  NewCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    NewCtrl = $controller 'NewCtrl', {
      $scope: scope
    }

