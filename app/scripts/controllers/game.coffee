'use strict'

angular.module('nameGameApp')
  .controller 'GameCtrl', ($scope, $routeParams, $cookies, $location, angularFire) ->
    $scope.gameId = $routeParams.gameId
    $scope.playerName = $cookies.playerName
    ref = new Firebase("https://namegame.firebaseio.com/games/"+$scope.gameId)
    angularFire(ref, $scope, "game")

    $scope.respond = () ->
      game = $scope.game
      nextTurnIndex = (game.players.indexOf(game.currentTurn) + 1) % game.players.length
      game.currentTurn = game.players[nextTurnIndex]
      game.lastPhrase = game.response
      game.lastCharacter = game.response[game.response.length - 1] # Firebase limitation: no way to get last character of phrase during validation
      game.lastTurn = $scope.playerName
      game.response = ""

      # DEMO
#      # Store phrase
#      if !game.plays
#        game.plays = []
#      plays = game.plays
#      plays.push({
#        player: game.lastTurn,
#        phrase: game.lastPhrase
#      })
      # /DEMO


    $scope.boot = () ->
      if (confirm("Are you sure you want to boot " + $scope.game.currentTurn + "?"))
        $scope.extractCurrentlyPlayingPlayer()

    $scope.surrender = () ->
      $scope.extractCurrentlyPlayingPlayer()

    $scope.extractCurrentlyPlayingPlayer = () ->
      game = $scope.game
      turnIndex = game.players.indexOf(game.currentTurn)
      nextTurnIndex = (turnIndex + 1) % game.players.length
      game.currentTurn = game.players[nextTurnIndex]
      game.players.splice(turnIndex, 1)

    $scope.joinGame = () ->
      game = $scope.game
      players = game.players
      if !players
        game.players = []
        players = game.players
      if players.indexOf($scope.playerName) < 0
        players.push($scope.playerName)

    $scope.closeGame = () ->
      game = $scope.game
      game.archived = true
      $location.url('/games')