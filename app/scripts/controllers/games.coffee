'use strict'

angular.module('nameGameApp')
  .controller 'GamesCtrl', ($scope, $location, $cookies, angularFire, auth) ->
    $scope.playerName = $cookies.playerName
    ref = new Firebase("https://namegame.firebaseio.com/games")
    angularFire(ref, $scope, "games")

    $scope.enterGame = (gameId) ->
      $location.url('/game/'+gameId)

    $scope.startNewGame = () ->
      $location.url('/new')

    $scope.logout = () ->
      auth.logout()
      $location.url('/play')

    $scope.activeGames = () ->
      result = {}
      angular.forEach($scope.games, (value, key) ->
        if (!value.archived)
          result[key] = value
      )
      result

    $scope.noActiveGames = () ->
      Object.keys($scope.activeGames()).length == 0