'use strict'

angular.module('nameGameApp')
  .controller 'NewCtrl', ($scope, $cookies, $location, angularFire) ->
    $scope.playerName = $cookies.playerName
    ref = new Firebase("https://namegame.firebaseio.com/games")
    angularFire(ref, $scope, "games")

    # RFC1422-compliant Javascript UUID function. Generates a UUID from a random
    # number (which means it might not be entirely unique, though it should be
    # good enough for many uses). See http://stackoverflow.com/questions/105034
    $scope.uuid = ->
      'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) ->
        r = Math.random() * 16 | 0
        v = if c is 'x' then r else (r & 0x3|0x8)
        v.toString(16)
      )

    $scope.create = () ->
      gameId = $scope.uuid()
      game = {
        name: $scope.name,
        currentTurn: $scope.playerName,
        category: $scope.category,
        players: [$scope.playerName]
      }
      console.log(game)
      $scope.games[gameId] = game
      $location.url('/game/'+gameId)