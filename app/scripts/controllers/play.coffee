'use strict'

angular.module('nameGameApp')
.controller 'PlayCtrl', ($scope, $location, auth) ->
    $scope.login = (provider) ->
      auth.login(provider)
