'use strict'

angular.module('nameGameApp')
.controller 'NavigationCtrl', ($scope, $location, $cookies, auth) ->
    $scope.currentPlayerName = $cookies.playerName

    $scope.$watch(
      () -> return $cookies.playerName
      (newValue) ->
        console.log('New player: ' + newValue)
        $scope.currentPlayerName = newValue
    )

    $scope.logOut = () ->
      if (confirm('Are you sure you want to quit?'))
        auth.logout()
        $cookies.playerName = ''
        $location.path('/')

    $scope.getClass = (path) ->
      return "active" if $location.path().substr(0, path.length) == path
      return "";