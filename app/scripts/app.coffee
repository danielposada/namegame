'use strict'

class AuthService
	constructor: (location, scope, cookies) ->
		ref = new Firebase('https://namegame.firebaseio.com')
		@auth = new FirebaseSimpleLogin(ref, (error, user) ->
			if error
				console.log(error)
			else if user
				console.log('User is logged in')
				console.log(user)
				cookies.playerName = user.displayName
				location.path('/games')
				scope.$apply()
			else
				console.log('User is logged out')
				location.path('/play')
				scope.$apply()
			)

	login: (provider) ->
		@auth.login(provider)

	logout: () ->
		@auth.logout()

module = angular.module('nameGameApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'firebase'
])
module.config ($routeProvider) ->
  $routeProvider
  .when '/',
      templateUrl: 'views/main.html'
      controller: 'MainCtrl'
  .when '/play',
      templateUrl: 'views/play.html'
      controller: 'PlayCtrl'
  .when '/games',
      templateUrl: 'views/games.html'
      controller: 'GamesCtrl'
  .when '/game/:gameId',
      templateUrl: 'views/game.html'
      controller: 'GameCtrl'
  .when '/new',
      templateUrl: 'views/new.html'
      controller: 'NewCtrl'
  .otherwise
      redirectTo: '/'
module.factory('auth', ['$location', '$rootScope', '$cookies', (loc, scope, cookies) -> new AuthService(loc, scope, cookies)])
